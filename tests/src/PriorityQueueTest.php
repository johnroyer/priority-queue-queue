<?php

namespace Zeroplex\Test;

use PHPUnit\Framework\TestCase;
use Zeroplex\PriorityQueue;

class PriorityQueueTest extends TestCase
{
    private $pq = null;

    public function setup()
    {
        $this->pq = new PriorityQueue();
    }

    public function tearDown()
    {
        $this->pq = null;
    }

    public function testCountJustAfterInitialized()
    {
        $this->assertEquals(0, $this->pq->count());
    }

    public function testInsertOneElement()
    {
        $this->pq->insert(1);

        $this->assertEquals(
            1,
            $this->pq->count()
        );

        return $this->pq;
    }

    /**
     * @depends testInsertOneElement
     */
    public function testIteratingOnce($pq)
    {
        $count = 0;

        foreach ($pq as $item) {
            $count++;
        }

        // only 1 item at this time
        $this->assertEquals(1, $count);
    }

    /**
     * @depends testInsertOneElement
     */
    public function testExtract($pq)
    {
        $this->assertEquals(
            1,
            $this->pq->extract()
        );
    }

    public function testQueueIsEmpty()
    {
        $this->assertTrue($this->pq->isEmpty());

        return $this->pq;
    }

    /**
     * @depends testQueueIsEmpty
     */
    public function testQueueIsNotQmpty($pq)
    {
        $pq->insert(1);

        $this->assertFalse($pq->isEmpty());
    }

    public function testDequeueWhileItemsInSamePriority()
    {
        $list = [1, 2, 3];

        foreach ($list as $item) {
            $this->pq->insert($item);
        }

        // extract() order should be: 1, 2, 3
        foreach ($list as $item) {
            $this->assertEquals(
                $item,
                $this->pq->extract()
            );
        }
    }

    public function testDeQueueWithDifferentPriority()
    {
        $this->pq->insert(1, 100);
        $this->pq->insert(2, 0);
        $this->pq->insert(3, -100);

        // number is smaller, priority is higher
        $this->assertEquals(3, $this->pq->extract());
        $this->assertEquals(2, $this->pq->extract());
        $this->assertEquals(1, $this->pq->extract());
    }

    /**
     * Priority should be an integer
     *
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidPriorityInput()
    {
        $this->pq->insert(1, 'invalid priority');
    }
}
